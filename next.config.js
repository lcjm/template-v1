/** @type {import('next').NextConfig} */
const nextConfig = {
  images: {
    remotePatterns: [
      {
        protocol: "https",
        hostname: "play.pokemonshowdown.com",
        port: "",
      },
    ],
  },
  reactStrictMode: true,
  swcMinify: true,
  env: {
    NEXT_API: process.env.NEXT_API,
  },
};

module.exports = nextConfig;
