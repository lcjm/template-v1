import Card from "@/components/ui/card";
import Loader from "@/components/ui/loader";
import { fetchPokemons } from "@/config/api/services/home/pokemon";
import { ApiUser } from "@/config/models/user.type";
import { useEffect, useState } from "react";

const Home = () => {
  const [pokemons, setPokemons] = useState<ApiUser[]>([]);
  const [loading, setLoading] = useState(false);

  const getPokemons = async () => {
    setLoading(true);
    try {
      const pokemons = await fetchPokemons(50);
      setPokemons(pokemons);
    } catch (error) {
      return error;
    } finally {
      setLoading(false);
    }
  };

  useEffect(() => {
    getPokemons();
  }, []);

  return (
    <div className="w-full min-h-screen">
      {loading && (
        <div className="w-full h-full flex items-center justify-center">
          <Loader />
        </div>
      )}
      <div className="w-full grid grid-cols-1 lg:grid-cols-3 gap-[48px] mt-[128px] ">
        {!loading &&
          pokemons.map((item, index) => (
            <Card key={index + item.name} url={String(item.url)} />
          ))}
      </div>
    </div>
  );
};
export default Home;
