export const typesPokemon: { [key: string]: string } = {
  normal: "bg-[#A8A878]",
  fire: "bg-[#F08030]",
  water: "bg-[#6890F0]",
  grass: "bg-[#78C850]",
  electric: "bg-[#F8D030]",
  ice: "bg-[#98D8D8]",
  ground: "bg-[#E0C068]",
  flying: "bg-[#A890F0]",
  poison: "bg-[#A040A0]",
  fighting: "bg-[#C03028]",
  psychic: "bg-[#F85888]",
  dark: "bg-[#705848]",
  rock: "bg-[#B8A038]",
  bug: "bg-[#B3C143]",
  ghost: "bg-[#705898]",
  steel: "bg-[#B8B8D0]",
  dragon: "bg-[#7038F8]",
  fairy: "bg-[#FFAEC9]",
  default: "bg-[#A8A878]",
};
