import { OnlyUser, Pokedex } from "./../models/user.type";
import { ApiUser, User } from "@/config/models/user.type";

export const PokemonAdapter = (user: User): ApiUser[] => {
  return user.results.map((item) => ({ name: item.name, url: item.url }));
};

export const PokemonOnlyAdapter = (pokemon: Pokedex): OnlyUser => {
  return { name: pokemon.name, id: pokemon.id, types: pokemon.types };
};
