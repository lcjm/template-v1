import { userSlice } from "@/config/redux/slices/user.slice";
import { configureStore } from "@reduxjs/toolkit";
import { ApiUser } from "@/config/models/user.type";

export interface AppStore {
  user: ApiUser;
}

export default configureStore<AppStore>({
  reducer: {
    user: userSlice.reducer,
  },
});
