import { ApiUser } from "@/config/models/user.type";
import { PokemonAdapter } from "@/config/adapter/pokemon.adapter";

const NEXT_API = process.env.NEXT_API;

export const fetchPokemons = async (
  pokemonNumber: number
): Promise<ApiUser[]> => {
  return fetch(`${NEXT_API}/pokemon/?limit=${pokemonNumber}`)
    .then((res) => res.json())
    .then((res) => PokemonAdapter(res));
};
