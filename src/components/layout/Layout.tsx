import React from "react";
import Header from "./header";

interface LayoutProps {
  children?: React.ReactNode;
}
const Layout: React.FC<LayoutProps> = ({ children }) => {
  return (
    <div className="w-full h-full bg-[#F3F6FF] px-[48px] lg:px-[64px] py-[48px] ">
      <Header />
      {children}
    </div>
  );
};

export default Layout;
