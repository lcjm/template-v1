import React from "react";
import Text from "@/components/ui/text";

const Header: React.FC = () => {
  return (
    <header className="w-full px-[24px] py-[40px] rounded-[20px] bg-white flex items-center justify-center">
      <Text type="headlines"> POKEMON DEMO </Text>
    </header>
  );
};
export default Header;
