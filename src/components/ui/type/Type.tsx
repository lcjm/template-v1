import { typesPokemon } from "@/config/models/typePokemon.types";
import React from "react";
import Text from "@/components/ui/text";
import { Species } from "@/config/models/user.type";

interface TypeProps {
  type: Species;
}
const Type: React.FC<TypeProps> = ({ type }) => {
  const getBackground = (type: string): string =>
    typesPokemon[type] || typesPokemon.default;
  return (
    <div
      className={`px-[12px] py-[4px] rounded-[8px] ${getBackground(type.name)}`}
    >
      <Text className="text-white" type="headlines">
        {type.name}
      </Text>
    </div>
  );
};

export default Type;
