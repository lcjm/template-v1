import React from "react";
import Lottie from "react-lottie";
import animationData from "public/pokemon-loader.json";

const Loader = () => {
  const defaultOptions = {
    autoplay: true,
    loop: true,
    animationData: animationData,
  };
  return (
    <div className="w-full h-full flex items-center justify-center">
      <Lottie height={250} options={defaultOptions} width={250} />
    </div>
  );
};
export default Loader;
