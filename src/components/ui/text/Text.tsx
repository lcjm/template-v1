import React from "react";

interface TextProps {
  children?: React.ReactNode;
  type: "headlines" | "body";
  className?: string;
}

const Text: React.FC<TextProps> = ({ children, type, className }) => {
  const setClassNames = (type: string): string => {
    if (type === "headlines") {
      return "text-lg font-bold";
    }
    return "text-base font-normal";
  };
  return <p className={setClassNames(type) + " " + className}> {children} </p>;
};

export default Text;
