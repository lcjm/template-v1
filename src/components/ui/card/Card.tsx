import useFetchPokemon from "@/hooks/useFetchPokemon";
import Image from "next/image";
import React from "react";
import Loader from "../loader";
import Type from "../type";
import Text from "./../text/Text";

interface CardProps {
  url: string;
}

const Card: React.FC<CardProps> = ({ url }) => {
  const { data, loading } = useFetchPokemon(url);
  return (
    <div className="w-full relative rounded-[20px] shadow-lg px-[24px] h-[300px] lg:h-[250px] bg-white">
      {loading ? (
        <Loader />
      ) : (
        <div className="w-full h-full flex flex-col items-center">
          <div className="absolute top-[-30px] w-[70px] h-[70px]">
            <Image
              alt={""}
              fill
              src={`https://play.pokemonshowdown.com/sprites/xyani/${data?.name}.gif`}
            />
          </div>
          <Text className="mt-[50px]" type="headlines">
            Pokemon no° {data?.id}
          </Text>
          <Text className="mt-[12px]" type="body">
            {data?.name}
          </Text>
          <div className="w-full flex justify-center gap-[24px] mt-[24px] ">
            {data?.types.map((item) => (
              <Type key={data?.name} type={item.type}></Type>
            ))}
          </div>
        </div>
      )}
    </div>
  );
};
export default Card;
