import { PokemonOnlyAdapter } from "@/config/adapter/pokemon.adapter";
import { OnlyUser, Pokedex } from "@/config/models/user.type";
import React from "react";

const useFetchPokemon = (url: string) => {
  const [data, setData] = React.useState<OnlyUser>();
  const [loading, setLoading] = React.useState(true);

  const getData = async () => {
    setLoading(true);
    try {
      const dataPokemon = await fetch(url);
      const pokemon: Pokedex = await dataPokemon.json();
      setData(PokemonOnlyAdapter(pokemon));
    } catch (error) {
      return error;
    } finally {
      setLoading(false);
    }
  };

  React.useEffect(() => {
    getData();
  }, []);

  return { data, loading };
};

export default useFetchPokemon;
